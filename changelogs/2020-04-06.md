**Disclaimer** During beta period some specific systems might be not working as intended, not working at all or abusable, for any abusable bugs found we will ensure all of it effect is reverted back and if necessary rollback characters.

# Changes
* `@autoloot` has been supressed by `@arealoot` it works very similar to greed, enabling it will allow you to automatically loot all loots within 7x7 area upon picking up 1 item
* The following items has been hard set to x1 rate in drops to stop the abuse of it high sell value through either high rate or easy accessible monsters
	* Sea-Otter Fur
	* Sardonyx
	* Sapphire
	* Glass Bead
	* Little Evil Wing
	* Dragon Scale
	* Yggdrasil Leaf
	* Red Frame
	* Alice's Apron
