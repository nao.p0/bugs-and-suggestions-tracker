# Welcome to Ragnarok: Project Zero bug tracker
![Ragnarok: Project Zero](.resources/logo.png)

## Ragnarok: Project Zero Main Website and Downloads

If you're just looking for Ragnarok: Project Zero or want to download the client, please head to [https://playragnarokzero.com](https://playragnarokzero.com)

## Bug and Feature Tracker

If you're looking for **bug reports** or **feature suggestions** concerning Project Zero, this is the right place!

You can [browse the **existing issues**](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/issues) or [create a **new one**](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/issues/new).

If you prefer, you can see the existing issues as a [kanban board](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/-/boards) or view the [development milestones](https://gitlab.com/ragnarok-project-zero/bugs-and-suggestions-tracker/milestones).

Please note that in order to post new issues, you'll need to [sign in or sign up for a free GitLab account](https://gitlab.com/users/sign_in?redirect_to_referer=yes).
