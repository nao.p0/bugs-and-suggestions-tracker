# Changelogs for Patch 1.1.6
This patch delivers new cash shop costumes, QoL updates, new content and more skill updates and fixes. You'll also find a ton of bugfixes and improvements in this patch. Let's dive in.

# Content
## The kraken
![Kraken](.resources/kraken.jpg)

The realm of Byalan 6F is now open, introducing a new end-game dungeon, monsters and valuable cards! You can access this realm from an NPC at `/navi iz_dun04 130/234`

## Extra Life
Thanks to everyone who donated to our Extra Life team and special thanks to all the streamers who joined our team! Thanks to you all we were able to reach almost $1400 in total donations, which is more than our initial goal of $1000. And all of that in such short period! :)

The costumes and zero consumables box can be redeemed at `/navi prontera 150/150`.

# Cash Shop
Early november costumes have been removed and replaced with a new set of costumes. We're aware that many people are waiting for PayPal to be able to donate and we're working on it, though it will take some time. When we launch PayPal as an option, we'll do a flash back sale with all of the old items for a limited time.

The new items will be available until at least the 1st of December:
* ![Golden Fish Hat](https://i.gyazo.com/884aa47d279cc51538a47161f0c1f8aa.gif)
* ![Forest Guide](https://i.gyazo.com/70e30dca414d4f1fc4582cbc2b3997f5.gif)
* ![Ghost Holiday](https://i.gyazo.com/57fd1dac230f6c6bc4aceb0973b3b55f.gif)
* ![Spider Temp](https://i.gyazo.com/0de52c8acf7ba14d84b50fcbda46496f.gif)
* ![Under Rimmed Glasses](https://i.gyazo.com/d7ac3491ad0b47dfc754214222498b69.gif)
* ![Stole of Dominion](https://i.gyazo.com/c8a711262d8d625805c9aa28c65f59da.gif)
* ![Kishu Dog](https://i.gyazo.com/c2a75e8922362b7391f53207bf715308.gif)
* ![Shih Tzu Hari](https://i.gyazo.com/17faa7ce27abfb7f5450b34f03f36bd5.gif)
* ![Angola Intention](https://i.gyazo.com/6896a0bd657e1709ae00a5f974e7b57b.gif)

# Quality of Life
* Fixed edge case where Attendance Reward Screen doesn't show on the first reward of the month
* Further improved map announcements to encourage fair play
* Improved WoE Damage Testing Arena to include Flee penalty <br>
---> **NOTE**: Critical Double Attacks work, however the animation doesn't display properly <br>
---> Added a note about pet intimacy behavior in WoE vs GvG Arena in NPC dialogue
* Added most (if not all) card pre/post fixes back :)
* Removed Heart Hairpin from tipbox (not in game)
* Fixed Cherokee NPC navigation
* Removed references to Gelstars from `Stone Refiner` and `Enchantment Staff` NPCs
* Shortened dialogue options with the `Stone Refiner` and `Enchantment Staff` NPCs
* Lowered Orc General Token requirement from 50 to 25 when evolving Orc Warrior to High Orc

# Fixes
* Fixed issue where **Authoritative Badge** with remaining time would disappear on re-log
* Fixed incorrect ATK value for **Hellfire Staff**: `110 -> 70` 
* Changed Monk Essence description to be consistent to use [Investigate] instead of [Occult Impact]
* Removed Greed Platinum Skill NPC
* Added `Mystic Frozen` as MVP reward to Deepsea Coelacanth
* Izlude Waterway Memorial Dungeon mobs will now die when all 4 clues have been activated
* Clicking "View" on the CT bosses' cards will no longer crash the client (sadly, they do not have illustrations)
* Corrected Whisper Pet Egg description to include LUK bonus
* Corrected respawn time of **Treasure Chest** in Guild Dungeons from `5 seconds` to `5 minutes`
* The following monster's spawns will no longer drop loot nor award exp: `Creamy Fear`, `Zombie Master`, `Ghostring`, `Angeling`, `Mastering`, `Vocal` and `Vagabond Wolf`
* Fixed issue where `Zealotus` and `Bloody Knight` could be summoned from a regular dead branch
* Fixed Droopy Turtle Hat's level requirement to be `lvl 60` and defense from `1` to `30`
* Fixed Hellfire Staff's initial bonus % on skill damage from `5%` to `10%`. (refine % remains unchanged)
* `Kingdom Coins` are now tradeable
* Corrected a problem in changing guild members' roles during the blockage of guild leader change
* Dual clienting inside WoE Castles is no longer possible


# Skill Fixes and Changes
* Fixed **Quagmire** from a `flat (5 x skill level) penalty` to apply a `capped stat penalty based on your total AGI/DEX` instead. See [wiki#Quagmire](https://wiki.playragnarokzero.com/wiki/Quagmire) for more details
* **Double Attack** now correctly applies hit adjustment for `1 HIT / level` when it procs
* Removed **Greed**, **Full Adrenaline Rush**, **Research Oridecon** from Blacksmith skill tree **[SKILL RESET AVAILABLE]**
* Removed **Flag Graffiti**, **Cleaner** from Rogue skill tree **[SKILL RESET AVAILABLE]**
* `Removed cast time` on **Rogue's Strip `<ARMOR_TYPE>`** skill
* Fixed **Envenom** damage scaling from a flat `[15ATK/lvl]` to `[15ATK%/lvl]`
* Changed **Ring of Nibelungen** cast time from `[0.5 Fixed + 1~5s cast time]` to `[0.5 Fixed + 5s Variable]`
* Changed **LoV** cooldown from `4.7s` to `2.7s`
* Changed **Meteor Storm** cooldown from up to `6.7s` to a static `2.2s`
* **Steal Coin** now has a `5 second cooldown`
* Changed **Ensemble Fatigue** from `30s` to `10s`
* **Brandish Spear** and **Combo Finish** now have STR scaling. It adds STR * 5% Attack ratio

# WoE
* Treasure boxes and Investment system has been enabled
