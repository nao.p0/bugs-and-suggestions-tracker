# Changelogs for Patch 1.1.0

This maintenance was mainly focused on delivering new content and various QoL improvements.

This maintenance marks the first content update since release, and we are already working hard on more content updates coming in the next few weeks.

## Content

### Headgears

#### Existing headgears
In Zero, most of the existing headgear crafting quests in the game have been completely rewritten.
These changes involve dedicated mini quests for each headgear, small EXP rewards, and sometimes adjusted ingredient lists.
For a more authentic Zero experience, we have started to implement these new headgear quests.

The following headgears can now be crafted using their new Zero quests (including a small EXP reward!):

Sakkat: Talk to the Trader in Morocc (`/navi morocc 127/59`)
Cake Hat: Talk to Lily in Lutie (`/navi xmas_in 178/177`)
Chef Hat: Talk to Lily in Lutie (`/navi xmas_in 178/177`)

Implementing all these new quests will take some time, but we already want you all to get familiar with the new NPC locations and ingredients.
So for all the remaining headgear quests we have also added temporary NPCs that will simply craft you the headgear in exchange for the ingredients - without the accompanying quest.

The temporary NPCs are:

* Mine_Helm: Talk to Minipas (`/navi geffen 83/189`)
* Pretend_Murdered: Talk to Peach (`/navi payon 130/67`)
* Mushroom_Band: Talk to Puppy (`/navi aldebaran 86/72`)
* Smokie_Hat: Talk to Insane Merchant (`/navi alberta 92/110`)
* Welding_Mask: Talk to Goldhand (`/navi geffen_in 144/166`)
* Plaster: Talk to Michael (`/navi payon 185/181`)
* Fashion_Sunglass: Talk to Faroe (`/navi comodo 214/184`)
* Hair_Brush: Talk to Nana (`/navi comodo 236/164`)
* Tulip_Hairpin: Talk to Cedric (`/navi aldebaran 240/164`)
* Mottled_Egg_Shell: Talk to Jongwa (`/navi morocc 217/225`)
* Candle: Talk to John (`/navi xmas 222/156`)
* Frog_Cap: Talk to Roda Frog (`/navi gef_fild07 278/218`)
* Binoculars: Talk to Barriojuna (`/navi payon 207/172`)
* Antler: Talk to Cherokee (`/navi alberta 122/53`)
* Doctor_Cap: Talk to Dana (`/navi izlude 83/205`)
* Light_Bulb_Band: Talk to Ollie (`/navi izlude 85/204`)
* Cross_Band: Talk to Redon (`/navi izlude 87/205`)
* Stripe_Band: Talk to Warden (`/navi izlude 89/204`)
* Blue_Hair_Band: Talk to Rita (`/navi izlude 85/207`)
* Mask_Of_Fox: Talk to Kitsune Mask Man (`/navi pay_arche 63/109`)
* Flower_Hairpin: Talk to Cinder (`/navi comodo 242/179`)
* Headband_Of_Power: Talk to Missionary (`/navi morocc 85/74`)
* Leaf_Headgear: Talk to Ranuka (`/navi gef_fild07 71/223`)
* Fish_On_Head: Talk to Tanja (`/navi comodo 300/230`)
* Party_Hat: Talk to Mananya (`/navi aldebaran 180/132`)
* Fur_Hat: Talk to Temin (`/navi geffen 207/163`)
* Headset: Talk to Senna (`/navi geffen_in 30/71`)
* Ear_Mufs: Talk to Swordsman (`/navi geffen_in 82/59`)
* Stop_Post: Talk to Carpenter (`/navi alberta 26/25`)
* Oxygen_Mask: Talk to George (`/navi payon_in02 25/71`)
* Boys_Cap_I: Talk to Grandfather (`/navi prontera 55/133`)
* Feather_Bonnet: Talk to Norbert (`/navi geffen 76/135`)
* Phantom_Of_Opera: Talk to Phantom (`/navi prontera 27/237`)
* Ph_D_Hat: Talk to Crawler (`/navi prontera 40/237`)

All NPCs that were used to craft these previously have been removed.

Our lovely Wiki team is already busy updating our wiki page with the new information: https://wiki.playragnarokzero.com/wiki/Headgear_Ingredients

#### Zero-exclusive headgears
But there's more! Zero also contains a lot of new exclusive headgears.
Most of these can be crafted in the newly opened "Atelier Manus", which you can find in north-west Prontera (`/navi prontera 50/290`).
You can find out what can be crafted by visiting the workshop and talking to Februs, or you open the Tipbox UI and search for "Atelier Manus".

Currently, the following headgears can be crafted, but more headgears (and costumes!) will follow in future updates:

* [Wanderer's Sakkat](https://cp.playragnarokzero.com/item/view/?id=19361)
* [Khalitzburg Helmet](https://cp.playragnarokzero.com/item/view/?id=19332)
* [Alice Wig](https://cp.playragnarokzero.com/item/view/?id=19356)
* [Clark Cap](https://cp.playragnarokzero.com/item/view/?id=19357)
* [Lude Hat](https://cp.playragnarokzero.com/item/view/?id=19359)
* [Lude Mask](https://cp.playragnarokzero.com/item/view/?id=19360)


## Changes
### Zeny nerfs
Items that were previously set to x1 drop rate for zeny nerf have been reverted back to drop at x5 drop rate.

* Pearl
* Sea-otter Fur
* Sardonyx
* Sapphire
* Glass Bead
* Little Evil Wing
* Yggdrasil Leaf
* Piece of Cake
* Red Frame
* Alice's Apron

### Blacksmith's Blessing
You will be able to craft those by buying a Blacksmith's Blessing recipe for 5 million zeny from Thor's Flame in addition to:
* 1 Mythril Ore
* 20 Oridecon
* 20 Elunium

Additionally, various monsters will drop Blacksmith's Blessing shards. Combining two of those will result in 1 Blacksmith's Blessing:
* All Champion monsters at 1%
* Payon/Geffen/Desert Fever monsters at 0.03%

### Random Refine Boxes
Refine boxes will be available for 4 million zeny from Thor's Flame.

### Rogue skill and stats resets
We will be offering a free skill/stat reset to all Rogues (please note this won't stack if you already have an unused skill/stat reset), due to latest changes to Rogue skills.

## Fixes
* Fixed an issue in Memorial Dungeon chests that caused them to sometimes drop fewer rewards than intended
* Fixed Wizard Essence Lv2 not triggering the flee bonus after casting Quagmire
* Fixed bonus for Alchemist Essence II Lv. 2, Priest Essence II Lv. 2, Monk Essence II Lv. 2 not triggering
* Fixed an issue with Poison Fog sometimes not spawning in the Ant Hell Memorial Dungeon
* Fixed Attendance UI not showing on player login
* Fixed Umbala language quest showing incorrect item requirements and have incorrect check for Giant Leaf
* Fixed item options affecting players when it should be Demi-Human only
* Fixed an issue where forged items refined by refine UI and similar won't give the forger fame points
