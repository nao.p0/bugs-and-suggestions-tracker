# Changelogs for Patch 1.3.2

This maintenance is focused on delivering a few changes and some important announcements.

## Fixes
* We improved the Instance re-warper to reduce chances of players temporarily losing access to their inventory/equipment
* Corrected item delay display message in client
* Corrected Incorrect PIN message on character deletion

## WoE
As many of you know we have been experimenting with WoE times for nearly 4 months now.
As we finally see the latest schedule is having the best attendance so far we'd like to announce that the current schedule will stay for the foreseeable future.

## PvP
The updates have been a bit slow since the release of the PvP Arena and we've been really off schedule with it.
But we plan to get things rolling again during April. Our plans for PvP are as the follow:

1. Ranking system (Will be based on ELO calculations)
2. PvP modes
	* 7 vs 7 pre-planned fights with different modes
3. Some sort of costume rewards and player titles based on their ranking

## Client update
We're planning on a new client to be released during April.

## Yuno
Our next Patch Yuno is already in testing phase, and we're expecting this update to come soon.
Because this content patch is particularly big, we would like to try a phased release approach for this, so that you don't have to wait as long to play new content. We're hoping such change will reduce the time players have to wait between updates.

## Cash shop
We will be doing a change to our cash shop releases policy as we've seen too many people complaining about very limited time releases.

* Items sold for 700 points or less will be added for a ~1 year release cycle from now on
	* (all items released before this maintenance will also be available again)
* Items sold for 900 points or more will stay limited for the 2~3 weeks cycle and be available through limited sales and/or some special systems that might be added later

For this second half of March we're releasing these costumes:
* Regular Release
	* ![Butterfly wing ears](https://i.gyazo.com/b402575a45bafb47cdccc6ae539191c7.gif)
	* ![Samambaia](https://i.gyazo.com/60f85c8a792b7527f5b61a07b9e345fa.gif)
	* ![Carmen Miranda's Hat](https://i.gyazo.com/16af3fc7ccd77e9c74bdaa84c4d50cc4.gif)
	* ![Hermos Cap](https://i.gyazo.com/7e4e92f317c4d8cddd6c35713e459c5b.gif)
	* ![Cherryblossom in mouth](https://i.gyazo.com/35873f91ac1effd0ef5b393d996af997.gif)
	* ![Chung Hairpin](https://i.gyazo.com/5d0839b91a63d23a81d1161605b760b4.gif)
	* ![Wind Prairie](https://i.gyazo.com/ec1b8bc9f2bd232d4c2e31fc0753c40e.gif)
* Limited Time
	* ![Happy Rabbit Ribbon](https://i.gyazo.com/2994b841ccc903ad55c1cd2538e8a6f2.gif)

